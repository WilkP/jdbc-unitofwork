package pl.edu.pjwstk.UnitOfWork;

import pl.edu.pjwstk.service.Entity;

/**
 * Created by vpnk on 19.11.16.
 */
public interface IUnitOfWorkRepository {

    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
