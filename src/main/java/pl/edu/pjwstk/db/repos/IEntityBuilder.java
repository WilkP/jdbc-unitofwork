package pl.edu.pjwstk.db.repos;

import pl.edu.pjwstk.service.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by vpnk on 22.11.16.
 */
public interface IEntityBuilder<TEntity extends Entity> {
        public TEntity build(ResultSet rs) throws SQLException;
}
