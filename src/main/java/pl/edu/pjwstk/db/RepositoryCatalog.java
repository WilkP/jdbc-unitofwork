package pl.edu.pjwstk.db;

/**
 * Created by vpnk on 11.11.16.
 */
public interface RepositoryCatalog {
    public IAddressRepository Iaddresses();
    public IClientDetailsRepository IclientDetails();
    public IOrderItemRepository IorderItems();
    public IOrderOrderItemMappingRepository IorderOrderItemMappings();
    public IOrderRepository Iorders();
}
