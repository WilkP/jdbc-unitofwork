package pl.edu.pjwstk.db;

import pl.edu.pjwstk.service.Address;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IAddressRepository extends IRepository<Address> {
    public List<Address> withStreet(String street);
    public List<Address> withBuildingNumber(String buildingNumber);
    public List<Address> withFlatNumber(String flatNumber);
    public List<Address> withPostalCode(String postalCode);
    public List<Address> withCity(String city);
    public List<Address> withCountry(String country);
}
