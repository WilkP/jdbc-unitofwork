package pl.edu.pjwstk.db;

import pl.edu.pjwstk.service.Address;
import pl.edu.pjwstk.service.ClientDetails;
import pl.edu.pjwstk.service.Order;
import pl.edu.pjwstk.service.OrderItem;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IOrderRepository extends IRepository<Order> {
    public List<Order> withClientDetailsId(int clientDetailsId);
    public List<Order> withDeliveryAddressId(int deliveryAddressId);
    //public List<Order> withItems(List<OrderItem> items);
}
