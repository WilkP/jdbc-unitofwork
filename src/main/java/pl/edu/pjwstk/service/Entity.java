package pl.edu.pjwstk.service;

/**
 * Created by vpnk on 19.11.16.
 */
public abstract class Entity {
    private long id;
    EntityState state;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

}
